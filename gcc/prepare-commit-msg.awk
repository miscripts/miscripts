# Awk script for use with my prepare-commit-msg git hook.

# When there is more than one ChangeLog in the commit, precede
# the entry by the directory name.
FNR == 1 && ARGC > 2 {
  printf "%s:\n\n", gensub("/ChangeLog$", "", "", FILENAME)
}

# Save the first line until we know if we want to print it.
FNR == 1 {
  authors = $0
  backport_info = ""
}

# If second line is blank there is only one patch author, maybe omit it.
FNR == 2 && /^$/ {
  cmd = "git config user.email"
  cmd | getline email
  if (authors !~ email) {
    print authors
    print
  }
  close(cmd)
  authors = ""
  next
}

# Stop processing the file at the next dated entry.
FNR > 3 && /^20[1-9][0-9]-[01][0-9]-[0-3][0-9]  / {
  print hold "\n"
  nextfile
}

FNR >= 3 && /^\tBackport/ {
  gsub("^\t", "")
  backport_info = $0
  next
}

FNR >= 2 && /^$/ {
  if (authors != "") {
    print authors
    print
    authors = ""
  }
  else if (backport_info != "") {
    print backport_info
    print
    backport_info = ""
  }
  else {
    print hold "\n"
    hold = ""
    nextfile
  }
  hold = ""
}

FNR >= 2 && /^./ {
  if (authors != "")
    authors = authors "\n" $0
  else if (backport_info != "") {
    gsub("^\t", "")
    backport_info = backport_info "\n" $0
  }
  else {
    if (length(hold))
      hold = hold "\n"
    hold = hold $0
  }
}
