Name:		cplusplus-draft-env
Version:	1
Release:	1%{?dist}
Summary:	Configure environment for editing C++ working draft 

License:	None
URL:		http://github.com/cplusplus/draft/
BuildArch:	noarch

Requires:	texlive texlive-isodate texlive-relsize texlive-ulem texlive-fixme
Requires:	texlive-extract texlive-imakeidx texlive-splitindex
Requires:	latexmk
# Not required, but very useful
Requires:	diffpdf

%description
Metapackage for tools for editing the C++ working draft.


%prep
# empty

%build
# empty

%install
# empty

%files
# empty


%changelog
* Wed Jan 06 2016 Jonathan Wakely <jwakely@redhat.com> 1-1
- Initial package.
