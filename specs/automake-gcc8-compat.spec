Name:		automake-gcc8-compat
Version:	1.11.6
Release:	6%{?dist}
Summary:	A GNU tool for automatically creating Makefiles

License:	GPLv3+
URL:		https://www.gnu.org/software/automake/
Source0:	http://ftp.gnu.org/gnu/automake/automake-1.11.6.tar.xz

BuildArch:	noarch

BuildRequires:	make
# Copied from F23 automake.spec
Requires:	autoconf-gcc8-compat = 2.64
Requires:	perl(Thread::Queue)
Requires:	perl(threads)
BuildRequires:	perl-macros
BuildRequires:	perl(Thread::Queue)
BuildRequires:	autoconf >= 2.68
BuildRequires:	automake
BuildRequires:	help2man

Patch1:         automake-1.11.6-escape-brace.patch
Patch2:         automake-1.11.6-prototypes.patch

%description
Automake is a tool for automatically generating `Makefile.in'
files compliant with the GNU Coding Standards.

Automake version %{version} is required for GCC 8.x development.

%global debug_package %{nil}

%global short_ver 1.11

%prep
%setup -q -n automake-%{version}
%patch1 -p1
%patch2 -p1
autoreconf -iv

%global install_prefix /opt/autotools-gcc8

%build
echo $PATH
./configure --prefix=%{install_prefix} --program-suffix=_
make %{?_smp_mflags}


%install
%make_install
cat > ${RPM_BUILD_ROOT}%{install_prefix}/bin/automake << "EOT"
#!/usr/bin/bash
if [ -f %{install_prefix}/bin/autoconf_ ]; then
  export AUTOCONF=%{install_prefix}/bin/autoconf_
  export AUTOHEADER=%{install_prefix}/bin/autoheader_
fi
export AUTOMAKE=%{install_prefix}/bin/automake_
export ACLOCAL=%{install_prefix}/bin/aclocal_
export AUTOM4TE=%{install_prefix}/bin/autom4te_
name=${0##*/}
if [[ $name = *-%{short_ver} ]]; then
  name=${name%%-%{short_ver}}_-%{short_ver}
else
  name=${name}_
fi
exec %{install_prefix}/bin/${name}
EOT
chmod 755 ${RPM_BUILD_ROOT}%{install_prefix}/bin/automake
ln -s automake ${RPM_BUILD_ROOT}%{install_prefix}/bin/automake-%{short_ver}
ln -s automake ${RPM_BUILD_ROOT}%{install_prefix}/bin/aclocal
ln -s automake ${RPM_BUILD_ROOT}%{install_prefix}/bin/aclocal-%{short_ver}
ln -s automake ${RPM_BUILD_ROOT}%{install_prefix}/bin/autom4te
mkdir -m 755 ${RPM_BUILD_ROOT}%{install_prefix}/share/aclocal

%files
%{install_prefix}/bin/*
%{install_prefix}/share/aclocal
%{install_prefix}/share/aclocal-%{short_ver}/*
%{install_prefix}/share/automake-%{short_ver}/*
%exclude %{install_prefix}/share/doc
%exclude %{install_prefix}/share/info
%exclude %{install_prefix}/share/man

%changelog
* Tue Apr 19 2022 Jonathan Wakely <jwakely@redhat.com> - 1.11.6-6
- Add BR: make

* Wed Jul 18 2018 Jonathan Wakely <jwakely@redhat.com> - 1.11.6-5
- Fix wrapper script when invoked with version suffix.

* Mon May 21 2018 Jonathan Wakely <jwakely@redhat.com> - 1.11.6-4
- Add wrappers for automake-1.11 and aclocal-1.11

* Tue Jan 23 2018 Jonathan Wakely <jwakely@redhat.com> - 1.11.6-3
- Patch for aclocal prototypes

* Mon Jan 22 2018 Jonathan Wakely <jwakely@redhat.com> - 1.11.6-2
- Patch invalid regex

* Wed Jan 06 2016 Jonathan Wakely <jwakely@redhat.com> 1.11.6-1
- Initial package.
