Name:		mail-env
Version:	1
Release:	4%{?dist}
Summary:	Configure email environment

License:	None
Source0:	mailsync
BuildArch:	noarch

Requires:	mutt notmuch-mutt
Requires:	offlineimap krb5-workstation
Requires:	msmtp
Requires:	filesystem
# for mailcap text/html entry
Requires:       w3m elinks

%description
Metapackage for my email setup.

%prep
# empty

%build
# empty

%install
install -d $RPM_BUILD_ROOT/%{_usr}/local/bin
install -p -m 0755 %{SOURCE0} $RPM_BUILD_ROOT/%{_usr}/local/bin/

%post
echo "*******************************************************"
echo "* Run 'notmuch-new' to initialize email search index. *"
echo "*                                                     *"
echo "* Add mailsync to crontab e.g.                        *"
echo "*   */5 * * * * %{_usr}/local/bin/mailsync            *"
echo "*******************************************************"

%files
%{_usr}/local/bin/mailsync


%changelog
* Sun Jun 09 2019 Jonathan Wakely <jwakely@redhat.com> - 1-4
- Add w3m and elinks for text/html mailcap

* Mon Mar 25 2019 Jonathan Wakely <jwakely@redhat.com> - 1-3
- Redirect kinit -R error message to /dev/null

* Wed Jan 03 2018 Jonathan Wakely <jwakely@redhat.com> - 1-2
- Move comment onto its own line to avoid embedded newline in Source0.

* Wed Jan 06 2016 Jonathan Wakely <jwakely@redhat.com> 1-1
- Initial package.
