Name:		feels-env
Version:	1
Release:	3%{?dist}
Summary:	Configure general look&feel environment

License:	None
BuildArch:	noarch

Requires:	levien-inconsolata-fonts
# Plasma wants this but doesn't install it:
Requires:	google-noto-sans-fonts
# British spell-checking
Requires:	hunspell-en-GB
# Share mouse+keyboard across desktops
Requires:	synergy
# For OneRNG (also need to install onerng RPM)
Requires:       rng-tools at python-gnupg
Requires:       w3m

%description
Metapackage for general desktop customization.


%prep
# empty

%build
# empty

%install
# empty

%files
# empty


%changelog
* Sat Jun 08 2019 Jonathan Wakely <jwakely@redhat.com> - 1-3
- Add w3m

* Fri Mar 15 2019 Jonathan Wakely <jwakely@redhat.com> - 1-2
- Add OneRNG dependencies

* Fri Feb 26 2016 Jonathan Wakely <jwakely@redhat.com> - 1-1
- Initial package.
