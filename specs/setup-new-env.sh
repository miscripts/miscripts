#!/bin/bash

rel=$(awk -F: '{print $NF}' /etc/system-release-cpe)
arch=$(uname -m)

for i in feels-env dev-env fedora-dev-env
do
  rpm -q $i && continue
  mock --quiet --buildsrpm --spec ./$i.spec --sources . \
    && mock --quiet --no-clean --rebuild /var/lib/mock/fedora-${rel}-${arch}/result/$i-*.fc${rel}.src.rpm \
    && {
      ls /var/lib/mock/fedora-${rel}-${arch}/result/$i-*.fc${rel}.*.rpm | grep -F -v .src.rpm | xargs sudo dnf install -y
    }
done
for i in autoconf-gcc-compat automake-gcc-compat
do
  rpm -q $i && continue
  mock --quiet --buildsrpm --spec ./$i.spec --sources ~/rpmbuild/SOURCES/ \
    && mock --quiet --no-clean --rebuild /var/lib/mock/fedora-${rel}-${arch}/result/$i-*.fc${rel}.src.rpm \
    && {
      ls /var/lib/mock/fedora-${rel}-${arch}/result/$i-*.fc${rel}.*.rpm | grep -F -v .src.rpm | xargs sudo dnf install -y
    }
done
for i in gcc-env cplusplus-draft-env
do
  rpm -q $i && continue
  mock --quiet --buildsrpm --spec ./$i.spec --sources . \
    && mock --quiet --no-clean --rebuild /var/lib/mock/fedora-${rel}-${arch}/result/$i-*.fc${rel}.src.rpm \
    && {
      ls /var/lib/mock/fedora-${rel}-${arch}/result/$i-*.fc${rel}.*.rpm | grep -F -v .src.rpm | xargs sudo dnf install -y
    }
done
