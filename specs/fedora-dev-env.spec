Name:		fedora-dev-env
Version:	1
Release:	3%{?dist}
Summary:	Configure environment for Fedora packaging

License:	None
URL:            http://fedoraproject.org/
BuildArch:      noarch

Requires:	fedpkg fedora-packager fedora-packager-kerberos mock copr-cli
Requires:	python3-rpm-macros

%description
Metapackage for Fedora packaging tools.


%prep
# empty

%build
# empty

%install
# empty


%files
# empty


%changelog
* Thu Jul 14 2022 Jonathan Wakely <jwakely@redhat.com> - 1-3
- Add packages for python3_version macro and fkinit command
- Remove outdated post-installation message

* Sat Jun 08 2019 Jonathan Wakely <jwakely@redhat.com> - 1-2
- Add copr-cli

* Wed Jan 06 2016 Jonathan Wakely <jwakely@redhat.com> 1-1
- Initial package.
