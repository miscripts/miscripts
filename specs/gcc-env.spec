Name:		gcc-env
Version:	1
Release:	11%{?dist}
Summary:	Configure GCC development environment

License:	None
URL:		https://gcc.gnu.org/

Requires:	gcc-c++
Requires:	libstdc++-static%{?_isa} glibc-static%{?_isa}
Requires:	gmp-devel%{?_isa} mpfr-devel%{?_isa} libmpc-devel%{?_isa}
Requires:	isl-devel%{?_isa}
Requires:	flex dejagnu
Requires:	zlib-devel%{?_isa} libzstd-devel%{?_isa}
Requires:	git
Requires:	patchutils
Requires:	texinfo
Requires:	python3-unidiff python3-GitPython
# Use 'dnf copr enable jwakely/autotools-gcc' for these:
Requires:	automake-gcc-compat autoconf-gcc-compat
Requires:       git-email msmtp

# For libstdc++ documentation
Recommends:	doxygen graphviz
Recommends:	libxml2 libxslt
Recommends:	dblatex docbook5-style-xsl docbook5-schemas docbook2X
Recommends:	texlive-latex-bin-bin texlive-makeindex-bin
Recommends:	texlive-xtab texlive-adjustbox texlive-tocloft texlive-tabu
Recommends:	texlive-hanging texlive-stackengine texlive-newunicodechar
Recommends:	texlive-etoc

# TODO make this work for ppc64 etc. as well
%ifarch x86_64
%global isa32 (%{__isa_name}-32)
Requires: glibc-devel%{isa32} glibc-static%{isa32}
Requires: libgcc%{isa32} libstdc++%{isa32} libstdc++-static%{isa32}
%endif

%description
Metapackage for development tools for GCC and libstdc++ development.


%prep
# empty

%build
# empty

%install
# empty


%files
# empty


%changelog
* Tue Jun 28 2022 Jonathan Wakely <jwakely@redhat.com> - 1-11
- Add more texlive packages for libstdc++ docs

* Thu Jun 23 2022 Jonathan Wakely <jwakely@redhat.com> - 1-10
- Add requirements for git-send-email

* Fri Oct 02 2020 Jonathan Wakely <jwakely@redhat.com> - 1-9
- Remove glibc-headers, add 32-bit glibc-static and libstdc++-static

* Thu Jul 09 2020 Jonathan Wakely <jwakely@redhat.com> - 1-8
- Add _isa suffix to devel packages

* Thu Jun 18 2020 Jonathan Wakely <jwakely@redhat.com> - 1-7
- Add Python modules for changelog utilities and remove git-merge-changelog
- Remove git-svn and cvs
- Change libstdc++ documentation tools to weak dependencies

* Wed Dec 04 2019 Jonathan Wakely <jwakely@redhat.com> - 1-6
- Add libzstd-devel dependency

* Wed Jun 12 2019 Jonathan Wakely <jwakely@redhat.com> - 1-5
- Add glibc-static dependency

* Fri Feb 22 2019 Jonathan Wakely <jwakely@redhat.com> - 1-4
- Add texlive-tabu dependency

* Wed Jan 02 2019 Jonathan Wakely <jwakely@redhat.com> - 1-3
- Add dependency on libstdc++-static

* Tue Jan 09 2018 Jonathan Wakely <jwakely@redhat.com> - 1-2
- Remove dependency on 32-bit glibc-headers due to pagure.io/releng/issue/7071

* Wed Jan 06 2016 Jonathan Wakely <jwakely@redhat.com> 1-1
- Initial package.
