Name:		autoconf-gcc-compat
Version:	2.69
Release:	3%{?dist}
Summary:	A GNU tool for automatically configuring source code

License:	GPLv3+
URL:		https://www.gnu.org/software/automake/
Source0:	http://ftp.gnu.org/gnu/autoconf/autoconf-2.69.tar.xz

BuildArch:	noarch

BuildRequires:	make
# Copied from F23 autoconf.spec
BuildRequires:	m4 >= 1.4.14
Requires:	m4 >= 1.4.14
%if 0%{?fedora} || 0%{?rhel}
BuildRequires:	perl-generators
%endif
BuildRequires:	perl-macros
BuildRequires:	perl(Data::Dumper)
BuildRequires:	perl(Text::ParseWords)
BuildRequires:	perl(Getopt::Long)
BuildRequires:	perl(File::Compare)
BuildRequires:	perl(File::Copy)
BuildRequires:	perl(DynaLoader)

%description
GNU's Autoconf is a tool for configuring source code and Makefiles.
Using Autoconf, programmers can create portable and configurable
packages, since the person building the package is allowed to
specify various configuration options.

Autoconf version %{version} is required for GCC trunk development.

%global debug_package %{nil}

%prep
%setup -q -n autoconf-%{version}

%global install_prefix /opt/autotools-gcc

%build
./configure --prefix=%{install_prefix} --program-suffix=_
# not parallel safe
make


%install
%make_install
cat > ${RPM_BUILD_ROOT}%{install_prefix}/bin/autoconf << EOT
#!/usr/bin/bash
export AUTOCONF=%{install_prefix}/bin/autoconf_
export AUTOHEADER=%{install_prefix}/bin/autoheader_
if [ -f %{install_prefix}/bin/automake_ ]; then
  export AUTOMAKE=%{install_prefix}/bin/automake_
  export ACLOCAL=%{install_prefix}/bin/aclocal_
  export AUTOM4TE=%{install_prefix}/bin/autom4te_
fi
exec %{install_prefix}/bin/\${0##*/}_
EOT
chmod 755 ${RPM_BUILD_ROOT}%{install_prefix}/bin/autoconf
ln -s autoconf ${RPM_BUILD_ROOT}%{install_prefix}/bin/autoheader
ln -s autoconf ${RPM_BUILD_ROOT}%{install_prefix}/bin/autoreconf

%files
%{install_prefix}/bin/*
%{install_prefix}/share/autoconf/*
%exclude %{install_prefix}/share/info
%exclude %{install_prefix}/share/man

%changelog
* Tue Apr 19 2022 Jonathan Wakely <jwakely@redhat.com> - 2.69-3
- Add BR: make

* Thu Jun 18 2020 Jonathan Wakely <jwakely@redhat.com> - 2.69-2
- Add missing Perl modules required for rawhide

* Fri Feb 15 2019 Jonathan Wakely <jwakely@redhat.com> - 2.69-1
- Update to 2.69

* Wed Jan 06 2016 Jonathan Wakely <jwakely@redhat.com> 2.64-1
- Initial package.
