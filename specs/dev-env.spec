Name:		dev-env
Version:	1
Release:	3%{?dist}
Summary:	Configure general development environment

License:	None

Requires:	vim-enhanced vim-X11
Requires:	gcc-c++
Requires:	clang compiler-rt%{?_isa}
Requires:	libstdc++-devel%{?_isa} libstdc++-static%{?_isa}
Requires:	libcxx-devel%{?_isa} libcxx-static%{?_isa}
Requires:	libcxxabi-devel%{?_isa} libcxxabi-static%{?_isa}
Requires:	automake autoconf autogen
Requires:	make gdb
Requires:	valgrind%{?_isa}
Requires:	libubsan%{?_isa} libubsan-static%{?_isa}
Requires:	libasan%{?_isa} libasan-static%{?_isa}
Requires:	libtsan%{?_isa} libtsan-static%{?_isa}
Requires:	boost-devel%{?_isa}
Requires:	cmake
Requires:	wget
Requires:	iotop perf strace

# TODO make this work for ppc64 etc. as well
%ifarch x86_64
%global isa32 (%{__isa_name}-32)
Requires: libgcc%{isa32} libstdc++%{isa32}
Requires: glibc-devel%{isa32} glibc-static%{isa32}
Requires: libstdc++-devel%{isa32} libstdc++-static%{isa32}
Requires: libcxx-devel%{isa32} libcxx-static%{isa32}
Requires: libcxxabi-devel%{isa32} libcxxabi-static%{isa32}
Requires: compiler-rt%{isa32}
Requires: valgrind%{isa32}
Requires: libubsan%{isa32} libubsan-static%{isa32}
Requires: libasan%{isa32} libasan-static%{isa32}
# N.B. no 32-bit libtsan
%endif

%description
Metapackage for general development tools.


%prep
# empty

%build
# empty

%install
# empty

%files
# empty


%changelog
* Thu Jul 09 2020 Jonathan Wakely <jwakely@redhat.com> - 1-3
- Add libstdc++ and libc++ packages

* Tue Nov 27 2018 Jonathan Wakely <jwakely@redhat.com> - 1-2
- Add make, sanitizer libraries, gdb and valgrind.

* Wed Jan 06 2016 Jonathan Wakely <jwakely@redhat.com> 1-1
- Initial package.
