Name:		autoconf-gcc8-compat
Version:	2.64
Release:	2%{?dist}
Summary:	A GNU tool for automatically configuring source code

License:	GPLv3+
URL:		https://www.gnu.org/software/automake/
Source0:	http://ftp.gnu.org/gnu/autoconf/autoconf-2.64.tar.bz2

BuildArch:	noarch

BuildRequires:	make
# Copied from F23 autoconf.spec
BuildRequires:	m4 >= 1.4.14
Requires:	m4 >= 1.4.14
BuildRequires:	perl-generators
BuildRequires:	perl-macros
BuildRequires:	perl(Data::Dumper)
BuildRequires:	perl(Text::ParseWords)
BuildRequires:	perl(Getopt::Long)

%description
GNU's Autoconf is a tool for configuring source code and Makefiles.
Using Autoconf, programmers can create portable and configurable
packages, since the person building the package is allowed to
specify various configuration options.

Autoconf version %{version} is required for GCC 8.x development.

%global debug_package %{nil}

%prep
%setup -q -n autoconf-%{version}

%global install_prefix /opt/autotools-gcc8

%build
./configure --prefix=%{install_prefix} --program-suffix=_
# not parallel safe
make


%install
%make_install
cat > ${RPM_BUILD_ROOT}%{install_prefix}/bin/autoconf << EOT
#!/usr/bin/bash
export AUTOCONF=%{install_prefix}/bin/autoconf_
export AUTOHEADER=%{install_prefix}/bin/autoheader_
if [ -f %{install_prefix}/bin/automake_ ]; then
  export AUTOMAKE=%{install_prefix}/bin/automake_
  export ACLOCAL=%{install_prefix}/bin/aclocal_
  export AUTOM4TE=%{install_prefix}/bin/autom4te_
fi
exec %{install_prefix}/bin/\${0##*/}_
EOT
chmod 755 ${RPM_BUILD_ROOT}%{install_prefix}/bin/autoconf
ln -s autoconf ${RPM_BUILD_ROOT}%{install_prefix}/bin/autoheader
ln -s autoconf ${RPM_BUILD_ROOT}%{install_prefix}/bin/autoreconf

%files
%{install_prefix}/bin/*
%{install_prefix}/share/autoconf/*
%exclude %{install_prefix}/share/info
%exclude %{install_prefix}/share/man

%changelog
* Tue Apr 19 2022 Jonathan Wakely <jwakely@redhat.com> - 2.64-2
- Add BR: make

* Wed Jan 06 2016 Jonathan Wakely <jwakely@redhat.com> 2.64-1
- Initial package.
