psf_impl() {
    local user="$1"
    shift
    local pattern="$*"
    if [[ -n $pattern ]]
    then
        pattern="[${pattern:0:1}]${pattern:1}"
    else
        pattern='.'
    fi
    case `uname` in
      SunOS) pscmd='/usr/ucb/ps auxww' ;;
      FreeBSD) pscmd='ps aufxww' ;;
      *) pscmd='ps auxww' ;;
    esac
    $pscmd | awk 'NR==1{h=$0} NR>1 && /'"${pattern//\//\\/}"'/ && $1 ~ "'$user'"{if(length(h)){ print h;h="";} print;} END{exit length(h)?1:0}';
}
# find processes that match supplied arguments
psf() {
    psf_impl '' "$*"
}
# similar, but finds processes owned by user "$2" (or self if no 2nd arg)
# usage: psfu [pattern] [user]
psfu() {
    local user
    local args="$*"
    if [ $# -lt 2 ]
    then
        user=$(id -un)
    else
        eval 'user=$'$#
        args=${args% $user}
    fi
    psf_impl "$user" "$args"
}

export_psf() { export -f psf_impl psf psfu ; }

# vim: ft=sh
