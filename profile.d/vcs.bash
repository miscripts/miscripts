vin() { vim -R "+set filetype=$*" -; }

vind() { vin diff ; }


### bazaaar

bzrdiff() { bzr diff "$@" | vind; }


### subversion

svndiff() { svn diff "$@" | vind; }
svnlog() { svn log "$@" | vin rcslog; }

fprune() { name=$1; shift ; find . \( -name "$name" -prune \) -o \( "$@" -print \) ; }
svnf() { fprune .svn "$@" ; }
svnff() { svnf "$@" -type f ; }
svng() { svnff | xargs grep "$@"; }

# vim: ft=sh

### perforce

# diff open files below current dir
p4diff() {
    for f in "${@:-.}"
    do
        if [ -d "$f" ]
        then
            local f=$( echo "$f" | sed 's#/*$#/...#' )
        fi
        p4 diff "$f"
    done 2>&1 | vin diff ;
}

p4diff2() { p4 diff2 -u "$@" 2>&1 | vin diff ; }

# generate unified diff for a changeset or an inclusive range of changesets
p4viewcs() {
    local from
    local to
    if [ $# -gt 1 ]
    then
        from="$1"
        to="$2"
    else
        from="$1"
        to="$1"
    fi
    from=$(( from - 1 ))
    p4diff2  ...@$from ...@$to
}


### CVS

# CVS diff with syntax highlighting
cvsdiff() { cvs diff "$@" | vin diff; }
# CVS log with syntax highlighting
cvslog() { cvs log "$@" | vin rcslog; }
# CVS update with conflicts highlighted
cvsupc() {
  highlight 'conflicts found in .*$' cvs up "$@"
  #cvs up $* 2>&1 | sed $'s/conflicts found in .*$/\033[31;04m&\033[0m/' >&2;
}
alias cvsstat='cvs status | fgrep File'

# vim: ft=sh
