# adapted from http://aaroncrane.co.uk/2009/03/git_branch_prompt/
# Not as full featured as the official Git prompt for Bash, or liquidprompt,
# but optimised for large repositories due to:
# - avoiding calling non-builtin commands where possible
# - not recalculating the prompt when no command is run (i.e. hitting Enter)
# - optionally caching the last status and only updating after a git command

# If GIT_PROMPT_STATUS and GIT_PS1_SHOWDIRTYSTATE are both empty, do nothing.

# If GIT_PROMPT_STATUS="short" only show a single '*' character to indicate
# there are uncommitted, unstaged or untracked changes, otherwise indicate
# which types of changes are present.

# If the file .git/git-prompt-cache exists then the prompt will only be updated
# if the first word of the previous command was 'git'.

# Indicate presence of staged (+), unstaged (*) and untracked (%) changes.
# Also indicate ahead/behind/diverged state of current branch and its upstream.
function git_status_summary() {
    # Indicates presence of staged/unstaged/untracked changes.
    local staged='+'
    local unstaged='*'
    local untracked='?'

    if [[ $GIT_PROMPT_STATUS = 'short' ]]; then
        # Just prints ' *' or nothing:
        git status -s | read && echo " $unstaged"
    else
        git status --porcelain -b | awk -v "I=$staged" -v "W=$unstaged" -v "U=$untracked" '
        NR == 1 && /^##/ {
            if (index($0, ", behind")) ab="d" ;
            else if (index($0, "[behind")) ab="b" ;
            else if (index($0, "[ahead")) ab="a" ;
            else ab="u" ;
        }
        /^[[:upper:]]/  { i=I }
        /^.[[:upper:]]/ { w=W ; next }
        /^\?\?/         { u=U ; exit 0 }
        END{ printf "%c%s%s%s", ab, i, w, u}'
    fi
}

# Set PS1_git_status to a summary of 'git status' output.
# Sets PS1_git_branch_status to a=ahead, b=behind, d=diverged, or u=up-to-date.
function find_git_status {
    # do nothing if GIT_PROMPT_STATUS is not set
    if [[ -z $GIT_PROMPT_STATUS && -z $GIT_PS1_SHOWDIRTYSTATE ]]; then
        unset PS1_git_status
        return
    fi

    # $1 should be the .git dir
    local cache=$1/git-prompt-cache

    # 'git status' can be slow so only run it after a new command
    local -a lastcmd=(`HISTTIMEFORMAT= history 1`)
    if ! [[ $PS1_git_last_cmd = ${lastcmd[0]} ]]; then
        PS1_git_last_cmd=${lastcmd[0]}
        if [ -w $cache ]; then
            # use .git/git-prompt-cache to cache status
            if ! [[ ${lastcmd[1]} = git ]]; then
                # use cached status
                PS1_git_status=$(< $cache)
            else
                # Find (and cache) new status after a git command.
                PS1_git_status=$(git_status_summary | tee $cache)
            fi
        else
            PS1_git_status=$(git_status_summary)
        fi
        PS1_git_branch_status=${PS1_git_status:0:1}
        PS1_git_status=${PS1_git_status:1}
    fi
}

# Set PS1_git_branch to name of current git branch
# and call find_git_status to set PS1_git_status.
# Doing it by hand is faster than `git rev-parse --git-dir`
# and `git rev-parse --symbolic-full-name HEAD` subprocesses.
function find_git_branch {
    local dir=. head gitdir
    until [ "$dir" -ef / -o "$dir" -ef ~ ]; do
        if [ -f "$dir/.git" ]; then
            gitdir=$(awk '/^gitdir:/{print $2}' $dir/.git)
        elif [ -f "$dir/.git/HEAD" ]; then
            gitdir=$dir/.git
        else
            dir="../$dir"
            continue
        fi

        head=$(< "$gitdir/HEAD")
        if [[ $head ]]; then
            if [[ $head == ref:\ refs/heads/* ]]; then
                PS1_git_branch="${head#*/*/}"
            else
                PS1_git_branch='(detached)'
            fi
            find_git_status $gitdir

            if [[ $GIT_PROMPT_STATUS = fancy ]]; then
                case $PS1_git_branch_status in
                    a) PS1_git_branch="⏫${PS1_git_branch}" ;;
                    b) PS1_git_branch="⏩${PS1_git_branch}" ;;
                    d) PS1_git_branch="🔀${PS1_git_branch}" ;;
                    # u) PS1_git_branch="${PS1_git_branch}=" ;;
                    *) ;;
                esac
                PS1_git_status="${PS1_git_status/\*/±}"
            fi
        else
            PS1_git_branch='(unknown)'
            PS1_git_status='!'
        fi
        if [[ $GIT_PROMPT_STATUS = fancy ]]; then
            if [ -f $gitdir/MERGE_HEAD ];then
                PS1_git_status="$PS1_git_status ⛙ "
            fi
            if [ -f $gitdir/CHERRY_PICK_HEAD ];then
                PS1_git_status="$PS1_git_status 🍒 "
            fi
            # PS1_git_branch=" $PS1_git_branch"
        fi
        return
    done
    PS1_git_branch=''
    PS1_git_status=''
}
if ! [[ "$PROMPT_COMMAND" =~ find_git_branch ]]; then
    PROMPT_COMMAND="find_git_branch; $PROMPT_COMMAND"
fi
PS1_git_orig=${PS1_git_orig:-$PS1}
PS1='${PS1_git_branch:+\[\e[32m\]$PS1_git_branch${PS1_git_status:+\[\e[31m\]$PS1_git_status}\[\e[m\] }'"$PS1_git_orig"

# vim:sw=4 et:
