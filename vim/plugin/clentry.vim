" clentry.vim
" Brief: Read the first entry from GNU-style ChangeLogs
" Version: 0.1
" Date: Jan 08, 2012
" Maintainer: Jonathan Wakely <redi@kayari.org>
"
" Installation: put this file under your ~/.vim/plugin/
"
" Usage:
"
" Copies the newest entry from GNU-style ChangeLog files into
" the current buffer, for use in commit log messages.
"
" :Clentry [dir] ...
"
" For each directory 'dir' open 'dir/ChangeLog' and read the
" first entry into the current buffer.
" If no arguments are given then the current directory is used.
"
" TODO scan buffer looking for ChangeLog files being committed.
"
" Revisions:
"
" 0.1, Jan 08, 2012:
"  * Initial version

" No attempt is made to support vim versions before 7.0.
if version < 700
    finish
endif

let g:clentry_filename = 'ChangeLog'

function! ReadChangeLogs(...) abort

    let filename = g:clentry_filename
    let dirs = a:0 > 0 ? a:000 : ['.']
    let pattern = '^\d\{4\}-\d\d-\d\d  .*  <.*@.*>'
    let maxlines = 100
    let pos = 0

    for d in dirs
        let f = globpath(d, filename)
        if !strlen(f)
            continue
        endif
        let entry = []
        let lines = readfile(f, '', maxlines)
        for line in lines
            if line =~ pattern
                if empty(entry)
                    let entry += [line]
                else
                    break
                endif
            elseif !empty(entry)
                let entry += [line]
            endif
        endfor

        if !empty(entry)
            if len(dirs) > 1
                let dir = substitute(f, '/'.filename.'$', '/', '')
                call append(pos, [dir])
                let pos += 1
            endif
            call append(pos, entry)
            let pos += len(entry)
        endif
    endfor
endfunction

command -nargs=* -complete=dir Clentry call ReadChangeLogs(<f-args>)

" vim: sw=4 ts=4 et
